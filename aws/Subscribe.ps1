﻿param(
    [Parameter(Mandatory=$true)][string]$EnvName,
    [Parameter(Mandatory=$true)][string]$Email
    )

$topicName = "homer-$EnvName-topic"

$topic = Get-SNSTopic|Where-Object -Property "TopicArn" -Like "*$topicName" # find the topic

Connect-SNSNotification -TopicArn $topic.TopicArn -Protocol "email" -Endpoint $Email
