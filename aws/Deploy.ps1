﻿
param(
    [Parameter(Mandatory=$true)][string]$EnvName,
    [switch]$IncludeCode = $false
    )

. .\Config.ps1 # import variables
$stackName = $stackName -f $EnvName
$codeFilename = $codeFilename -f (Get-Date -Format "yyyyMMdd-HHmmss")
$layerName = $layerName -f $EnvName

If ($IncludeCode) {
    # clean, create directories

    If (!(Test-Path -Path "$buildDir")) {
        New-Item -Type Directory -Path "$buildDir" -Force
    }
    Remove-Item -Path "$buildDir\*" -Recurse -Force


    # zip lambda code

    Compress-Archive -Path "$projectDir\src\*.py" -DestinationPath "$buildDir\$codeFilename" -CompressionLevel Optimal


    # upload to S3

    Write-S3Object -BucketName "arturm2-homer" -Key "$codeFilename" -File "$buildDir\$codeFilename" -Verbose
}
Else {
    $s3obj = Get-S3Object -BucketName arturm2-homer| Where-Object -Property "Key" -Like "homer-code-*.zip"| Sort-Object -Property "LastModified" -Descending| Select-Object -First 1
    $codeFilename = $s3obj.Key
    Write-Host "Using existing code: $codeFilename"
}


# figure out layer version (latest)

$layerVer = Get-LMLayerVersionList -LayerName $layerName| Sort-Object -Property Version| Select-Object -Last 1

$body = Get-Content -Path "$projectDir\homer-stack.json"|Out-String
$params = @{
    StackName = $stackName
    Capability = "CAPABILITY_NAMED_IAM"
    TemplateBody = $body
    Parameter = @( `
        @{ ParameterKey="EnvName"; ParameterValue=$EnvName }, `
        @{ ParameterKey="LayerArn"; ParameterValue=$layerVer.LayerVersionArn }, `
        @{ ParameterKey="CodeFilename"; ParameterValue=$codeFilename } `
    )
}

$stack = Get-CFNStack $stackName -ErrorAction SilentlyContinue
if ($stack) {
    Update-CFNStack @params
}
else {
    New-CFNStack @params
}
