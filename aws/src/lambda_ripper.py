import os

from homer.captor import Ripper
from homer.util import setup_logging

from dynamo import DynamoStorage
from ripper_handler import RipperHandler
from util import get_config

setup_logging(get_config(os.environ["HOMER_LOGGING_CONFIG_PATH"]))
config = get_config(os.environ["HOMER_CONFIG_PATH"])
ripper = Ripper(config["ripper"])
storage = DynamoStorage(config["dynamo_storage"])

handler = RipperHandler(ripper, storage)
