import datetime
import json

import boto3
from dateutil import tz


class SnsNotifier:

    def __init__(self, config):
        self.config = config

    @staticmethod
    def _now():
        return datetime.datetime.now(tz.gettz('Europe/Paris'))

    def notify(self, content):
        subject = self.config['subject'].format(self._now())

        client = boto3.client('sns')
        response = client.publish(
            TargetArn=self.config["sns_topic_arn"],
            Message=json.dumps({'default': content}),
            MessageStructure='json',
            Subject=subject
        )
