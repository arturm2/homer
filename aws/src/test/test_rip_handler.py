import asyncio
import unittest
from unittest.mock import Mock

from homer.test.test_runner import get_fake_ads

from ripper_handler import RipperHandler


def get_mock_coroutine(return_value):
    @asyncio.coroutine
    def mock_coroutine(*args, **kwargs):
        return return_value

    return Mock(wraps=mock_coroutine)


class MyTestCase(unittest.TestCase):

    def test_rip_handler(self):
        ripper_mock = Mock()
        ripper_mock.capture_ads.side_effect = get_mock_coroutine(get_fake_ads(unique=True))
        storage_mock = Mock()

        handler = RipperHandler(ripper_mock, storage_mock)
        result = handler(None, None)

        storage_mock.store_ads.assert_called_once()
        self.assertEqual(len(storage_mock.store_ads.call_args[0][0]), 9)


if __name__ == '__main__':
    unittest.main()
