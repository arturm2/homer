"""
Lambda Ripper handler
"""

import asyncio


class RipperHandler:
    """
    Lambda handler that rips the ads and stores
    """

    def __init__(self, ripper, storage):
        self.ripper = ripper
        self.storage = storage

    def __call__(self, event, context):
        ads = asyncio.run(self.ripper.capture_ads())
        self.storage.store_ads(ads)
