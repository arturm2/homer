from datetime import datetime

import boto3
from botocore.exceptions import ClientError

from homer import Ad


class DynamoStorage:

    def __init__(self, config):
        self.config = config
        self.db = boto3.resource('dynamodb', region_name=self.config['region_name'], endpoint_url=self.config['endpoint_url'])

    def store_ads(self, ads):
        table = self.db.Table(self.config["ad_table_name"])
        for ad in ads:
            try:
                table.put_item(
                    Item=DynamoStorage.ad_to_dict(ad),
                    ConditionExpression="attribute_not_exists(uri)")
            except ClientError as e:
                if e.response['Error']['Code'] == 'ConditionalCheckFailedException':
                    pass
                else:
                    raise e

    @staticmethod
    def ad_from_dynamo(dct):
        """
        Dynamo-specific Ad decoder.
        Note, it maps 'uri' to 'url'.
        """
        return Ad(dct["kind"]["S"],
                  dct["site"]["S"],
                  dct["uri"]["S"],
                  dct["id"]["S"],
                  dct["keyword"]["S"],
                  datetime.strptime(dct["discovered_at"]["S"], '%Y-%m-%dT%H:%M:%S.%f'))

    @staticmethod
    def ad_to_dict(ad):
        """
        Converts an Ad to a dictionary.
        Note, it maps 'url' to 'uri'.
        """
        return {
            'kind': ad.kind,
            'site': ad.site,
            'uri': ad.url,
            'id': ad.id,
            'keyword': ad.keyword,
            'discovered_at': ad.discovered_at.isoformat() if ad.discovered_at else None
         }