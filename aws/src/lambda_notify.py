import os

from homer.notifier import EmailNotifier, Formatter
from homer.util import setup_logging

from notify_handler import NotifyHandler
from sns import SnsNotifier
from util import get_config

setup_logging(get_config(os.environ["HOMER_LOGGING_CONFIG_PATH"]))
config = get_config(os.environ["HOMER_CONFIG_PATH"])
formatter = Formatter()

config['sns_notifier']['sns_topic_arn'] = os.environ["HOMER_SNS_TOPIC_ARN"] # patch sns_topic_arn
notifier = SnsNotifier(config['sns_notifier'])

handler = NotifyHandler(notifier, formatter)
