import json

import boto3


def get_config(parameter_path):
    """
    Gets a parameter value from parameter store
    """
    ssm = boto3.client('ssm')
    parameter = ssm.get_parameter(Name=parameter_path, WithDecryption=False)
    return json.loads(parameter['Parameter']['Value'])
