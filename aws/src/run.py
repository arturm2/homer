"""
Integration test of DynamoStorage
"""

from unittest.mock import Mock

from homer.test.test_runner import get_fake_ads
from homer.util import read_json_config
from homer.util import setup_logging

from dynamo import DynamoStorage
from ripper_handler import RipperHandler
from test.test_rip_handler import get_mock_coroutine


setup_logging(read_json_config('logging.json'))
config = read_json_config("config.json")

ripper_mock = Mock()
ripper_mock.capture_ads.side_effect = get_mock_coroutine(get_fake_ads(unique=True))
storage = DynamoStorage(config["dynamo_storage"])

handler = RipperHandler(ripper_mock, storage)
handler(None, None)
