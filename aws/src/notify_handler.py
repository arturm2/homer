import datetime
import json
import logging

import boto3
from dateutil import tz

from dynamo import DynamoStorage


class NotifyHandler:

    def __init__(self, notifier, formatter):
        self.notifier = notifier
        self.formatter = formatter
        self.log = logging.getLogger(self.__class__.__name__)

    def __call__(self, event, context):
        ads = self.grab_ads(event)
        self.log.info("received %d new ads", len(ads))

        if len(ads) > 0:
            content = self.formatter.get_text(ads)
            self.notifier.notify(content)

    @staticmethod
    def grab_ads(event):
        ads = []
        for record in event['Records']:
            if record['eventName'] == 'INSERT':
                item = record['dynamodb']['NewImage']
                ads.append(DynamoStorage.ad_from_dynamo(item))
        return ads
