﻿
$buildDir = "C:\***\homer\build"
$projectDir = "C:\***\homer\repo\aws"
$venvDir = "C:\***\.virtualenvs\homer-aws\Lib\site-packages"

$stackName = "homer-{0}" # EnvName
$layerFilename = "homer-layer-{0}.zip" # timestamp
$layerName = "homer-{0}-layer" # EnvName
$codeFilename = "homer-code-{0}.zip" # timestamp
