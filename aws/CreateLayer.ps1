﻿param(
    [Parameter(Mandatory=$true)][string]$EnvName
    )

$packages = @("aiohttp", "async_timeout", "attr", "chardet", "yarl", "beautifulsoup4", "bs4", "homer", "idna", "multidict", "soupsieve", "python-dateutil", "six")

. .\Config.ps1 # import varaibles
$stackName = $stackName -f $EnvName
$layerFilename = $layerFilename -f (Get-Date -Format "yyyyMMdd-HHmmss")
$layerName = $layerName -f $EnvName


# clean, create directories

If (!(Test-Path -Path "$buildDir")) {
    New-Item -Type Directory -Path "$buildDir" -Force
}
Remove-Item -Path "$buildDir\*" -Recurse -Force
New-Item -Type Directory -Path "$buildDir\python"


# copy packages and zip

Foreach ($package in $packages) {
    Copy-Item -Path "$venvDir\$package*" -Recurse -Destination "$buildDir\python"
}

Compress-Archive -Path "$buildDir\python" -DestinationPath "$buildDir\$layerFilename" -CompressionLevel Optimal


# clean up

Remove-Item -Path "$buildDir\python" -Recurse -Force


# create layer

$bytes = [System.IO.File]::ReadAllBytes("$buildDir\$layerFilename")
Publish-LMLayerVersion -LayerName $layerName -CompatibleRuntime "python3.7" -Content_ZipFile $bytes -Verbose
