import setuptools

packages = [
    'homer',
    'homer.captor',
    'homer.notifier',
    'homer.storage',
    'homer.test',
    'homer.test.captor'
]

package_data = {
    'homer': [
        'test/captor/data/gratka-ads.html',
        'test/captor/data/gratka-empty.html',
        'test/captor/data/otodom-ads.html',
        'test/captor/data/otodom-empty.html',
        'test/captor/data/morizon-ads.html',
        'test/captor/data/morizon-empty.html',
        'test/captor/data/gumtree-ads.html',
        'test/captor/data/gumtree-empty.html',
        'test/data/fake_ads.json',
        'test/data/fake_ads_unique.json'
    ]
}

setuptools.setup(
    name='homer',
    version='0.0.2',
    packages=packages,
    package_data=package_data,
    package_dir={'': '.\\src'},
    url='https://bitbucket.org/arturm2/homer',
    license='Read only licence: you can have a look, but please do not copy, use, distribute, etc.',
    author='arturm2',
    author_email='arturm2@o2.pl',
    description='Home hunter',
    install_requires=[
        'aiohttp',
        'beautifulsoup4',
        'python-dateutil'
    ]
)
