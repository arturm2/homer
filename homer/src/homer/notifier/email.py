"""
Email notifier module
"""
import datetime
import logging
import smtplib
import time

from dateutil import tz


class EmailNotifier:
    """
    Email plain text notifier: service sending email notification
    """

    def __init__(self, config):
        """
        :param config: configuration dictionary
        """
        self.log = logging.getLogger(self.__class__.__name__)
        self.config = config

    def _now(self):
        return datetime.datetime.now(tz.gettz('Europe/Paris'))

    def notify(self, content):
        """
        Sends notification actually
        """
        to_addrs = ','.join(self.config['recipients'])

        self.log.debug("Sending notification to: %s", to_addrs)

        subject = self.config['subject'].format(self._now())

        msg = self.body_template.format(to_addrs=to_addrs, from_email=self.config['from_email'], subject=subject, message=content)

        server = smtplib.SMTP_SSL(self.config['server_addr'])
        server.ehlo()
        server.login(self.config['server_username'], self.config['server_password'])
        server.sendmail(self.config['from_email'], self.config['recipients'], msg)
        server.quit()
        self.log.info("Notification sent to %s", to_addrs)

    body_template = """To: {to_addrs}
From: {from_email}
Subject: {subject}
X-Priority: 1 (Highest)

{message}
"""
