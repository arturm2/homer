"""
Formatter for a list of ads
"""
import io

from homer.ad import Ad


class Formatter:
    """
    Formatter for a list of ads
    """

    def get_text(self, ads):
        """
        Formats a list of ads ordering by kind and keyword
        """
        keywords = list(set(map(lambda a: a.keyword, ads)))
        keywords.sort(key=self._key)

        res = io.StringIO()

        for kind in [Ad.KIND_HOUSE, Ad.KIND_APART]:
            kind_ads = list(filter(lambda a: a.kind == kind, ads))
            if len(kind_ads) > 0:
                res.write('DOMY' if kind == Ad.KIND_HOUSE else 'MIESZKANIA')
                res.write('\n')
                for keyword in keywords:
                    keyword_ads = list(filter(lambda a: a.keyword == keyword, kind_ads))
                    if len(keyword_ads) > 0:
                        res.write(keyword + '\n')
                        for k in keyword_ads:
                            res.write(k.url + '\n')
                        res.write('\n')

        return res.getvalue()

    def _key(self, val):
        """
        Gets a mapping of a district name to number (for arbitrary sorting of ads)
        """
        if val in self.sort_mapping:
            return self.sort_mapping[val]
        else:
            return len(self.sort_mapping) + 1

    sort_mapping = {
        'biskupin': 1,
        'sepolno': 2,
        'zalesie': 3,
        'zacisze': 4,
        'dabie': 5,
        'bartoszowice': 6,
        'srodmiescie': 7
    }
