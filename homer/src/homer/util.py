"""
Helper methods
"""

import json
import logging
import logging.config
import os.path
import time


def get_filepath(script_file, filename):
    """
    Returns full file path of a file, by executing script path
    """
    _dir = os.path.dirname(os.path.abspath(script_file))
    return os.path.join(_dir, filename)


def read_json_config(filepath):
    """
    Reads a JSON file and returns a dictionary
    """
    with open(filepath, 'r') as fp:
        config = json.load(fp)
    return config


def setup_logging(config):
    """
    Basic logging setup
    """
    logging.basicConfig()
    logging.config.dictConfig(config)


def perf_calculate(t0):
    """
    Returns time elapsed since last perf_counter call formatted (for logging)
    """
    t1 = time.perf_counter()
    return "{:.3f}s".format(t1 - t0)
