"""
Runner (main orchestrator) module
"""

import asyncio
import logging
import time

from homer.util import perf_calculate


class Runner:
    """
    Main orchestrator
    """

    def __init__(self, ripper, storage, notifier, formatter):
        """
        :param ripper: Ripper service
        :param storage: Storage service
        :param notifier: Notifier service
        :param formatter: Formatter service
        """
        self.ripper = ripper
        self.storage = storage
        self.notifier = notifier
        self.formatter = formatter
        self.log = logging.getLogger(self.__class__.__name__)

    def go(self):
        """
        Makes runner go
        """
        t0 = time.perf_counter()
        ads = asyncio.run(self.capture_ads())
        self.log.info("Captured %d unique ads in %s", len(ads), perf_calculate(t0))

        t0 = time.perf_counter()
        new_ads = self.find_new_ads(ads)
        self.log.info("Found %d new ads, deduped against storage in %s", len(new_ads), perf_calculate(t0))

        if len(new_ads) > 0:
            t0 = time.perf_counter()
            self.storage.store_ads(new_ads)
            self.log.info("Stored new ads in %s", perf_calculate(t0))

            t0 = time.perf_counter()
            self.notify(new_ads)
            self.log.info("Sent notifications, in %s", perf_calculate(t0))

    async def capture_ads(self):
        """
        Captures all the ads
        """
        return await self.ripper.capture_ads()

    def find_new_ads(self, ads):
        """
        Deduplicates list of ads by url
        """
        urls = list(map(lambda ad: ad.url, ads))
        new_urls = self.storage.find_new_urls(urls)
        return [ad for ad in ads if ad.url in new_urls]

    def notify(self, new_ads):
        """
        Formats results and invokes notification
        """
        content = self.formatter.get_text(new_ads)
        self.notifier.notify(content)
