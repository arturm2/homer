"""
Advertisement instance DTO
"""

from datetime import datetime


class Ad:
    """Advertisement instance, of an apartment or a house"""

    KIND_APART: str = "APART"
    KIND_HOUSE: str = "HOUSE"

    def __init__(self, kind: str, site: str = None, url: str = None, _id: str = None, keyword: str = None, discovered_at: datetime = None):
        """
        :param kind: HOUSE or APART,
        :param site: ad website,
        :param url: ad url,
        :param _id: website's unique id,
        :param keyword: keyword used to discover this ad,
        :param discovered_at: discovery timestamp
        """
        self.kind = kind
        self.site = site
        self.url = url
        self.id = _id
        self.keyword = keyword
        self.discovered_at = discovered_at

    def __str__(self):
        """
        str representation of an Ad
        """
        return f"Ad [{self.kind}, {self.site}, {self.id}, {self.keyword}, {self.discovered_at.isoformat()}, {self.url}]"

    @staticmethod
    def from_dict(dct):
        """
        Creates an Ad from a dictionary.
        Useful to decode from JSON eg. json.loads('...', object_hook=Ad.from_dict, ...)
        """
        return Ad(dct["kind"],
                  dct["site"],
                  dct["url"],
                  dct["id"],
                  dct["keyword"],
                  datetime.strptime(dct["discovered_at"], '%Y-%m-%dT%H:%M:%S.%f'))

    @staticmethod
    def to_json(obj):
        """
        Helper to encode as JSON.
        eg. json.dumps(ad, default=Ad.default, ...)
        """
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, Ad):
            return obj.__dict__
        return None
