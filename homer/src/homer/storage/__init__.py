"""
Services related to persistent storage
"""

from .sqlite import SqliteStorage
