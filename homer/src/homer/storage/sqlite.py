"""
Sqlite storage module
"""

import sqlite3


class SqliteStorage:
    """
    Sqlite implementation of Storage service
    """

    def __init__(self, config):
        """
        :param config: configuration dictionary
        """
        self.config = config
        self.filepath = self.config['filepath']
        self.initialise()

    def get_conn(self):
        """
        Opens db connection
        """
        return sqlite3.connect(self.filepath)

    def initialise(self):
        """
        Initialises database: creates the table
        """
        conn = self.get_conn()
        with conn:
            conn.execute(self.house_def_sql)

    def store_ads(self, ads):
        """
        Stores ads
        """
        rows = map(lambda ad: (ad.kind, ad.site, ad.url, ad.id, ad.keyword, ad.discovered_at), ads)
        conn = self.get_conn()
        with conn:
            conn.executemany(self.house_insert_sql, rows)

    def find_new_urls(self, urls):
        """
        Returns list or urls not existing yet (not found in the db)
        """
        placeholders = ', '.join(['?'] * len(urls))
        sql = self.find_new_urls_sql.format(placeholders=placeholders)

        conn = self.get_conn()
        with conn:
            cur = conn.execute(sql, urls)
            existing_urls = [row[0] for row in cur]

        return [x for x in urls if x not in existing_urls]

    house_def_sql = """create table if not exists house (
                    kind varchar(5) not null,
                    site varchar(32),
                    url varchar(255) not null,
                    id varchar(255),
                    keyword varchar(32) not null,
                    discovered_at datetime not null)"""
    house_insert_sql = "insert into house (kind, site, url, id, keyword, discovered_at) values (?, ?, ?, ?, ?, ?)"
    find_new_urls_sql = "select url from house where url in ({placeholders})"
