"""
Otodom implementation of Captor
"""
import re

from .captor import Captor


class OtodomCaptor(Captor):
    """
    Otodom implementation of Captor
    """

    site_name = 'otodom'

    def get_url(self, **kwargs):
        url = super(OtodomCaptor, self).get_url(district_id=self.get_name_mapping(self.keyword))
        return url

    def find_empty_marker(self):
        return self.get_soup().find(
            name='p',
            text=re.compile(u"Niestety nie znaleźliśmy ogłoszeń odpowiadających Twoim kryteriom"),
            attrs={'class': 'title'})

    def find_ads(self):
        items = self.get_soup().find_all('article', attrs={'class': 'offer-item'})

        ads = []
        for item in items:
            if item.attrs['data-featured-name'] != 'listing_no_promo':
                continue
            url = item.attrs['data-url']
            _id = item.attrs['data-item-id']
            ad = self.create_ad(url, _id)
            ads.append(ad)

        return ads

    def get_name_mapping(self, name):
        """
        Gets Otodom specific mapping for district's name
        """
        if name in self.district_map:
            return self.district_map[name]
        raise KeyError(f"There is no mapping for district '{name}'")

    def get_house_url(self):
        return "https://www.otodom.pl/sprzedaz/dom/wroclaw/{keyword}/?search%5Bfilter_float_price%3Afrom%5D={priceMin}&search%5Bfilter_float_price%3Ato%5D={priceMax}&search%5Bdist%5D=0&search%5Bdistrict_id%5D={district_id}&search%5Bsubregion_id%5D=381&search%5Bcity_id%5D=39&search%5Border%5D=created_at_first%3Adesc"

    def get_apart_url(self):
        return "https://www.otodom.pl/sprzedaz/mieszkanie/wroclaw/{keyword}/?search%5Bfilter_float_price%3Afrom%5D={priceMin}&search%5Bfilter_float_price%3Ato%5D={priceMax}&search%5Bfilter_float_m%3Afrom%5D=80&search%5Bdist%5D=0&search%5Bdistrict_id%5D={district_id}&search%5Bsubregion_id%5D=381&search%5Bcity_id%5D=39&search%5Border%5D=created_at_first%3Adesc"
