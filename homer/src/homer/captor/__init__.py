"""
Ripper and captors module
Captors download and parse specific websites.
Ripper is an entry point to launch captors.
"""


from .ripper import Ripper
from .otodom import OtodomCaptor
from .morizon import MorizonCaptor
from .gumtree import GumtreeCaptor
from .gratka import GratkaCaptor
