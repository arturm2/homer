"""
Morizon implementation of Captor
"""
import re
from abc import ABC

from .captor import Captor


class MorizonCaptor(Captor, ABC):
    """
    Morizon implementation of Captor
    """

    site_name = 'morizon'

    def find_empty_marker(self):
        return self.get_soup().find(
            name='div',
            text=re.compile(u"Brak ogłoszeń spełniających Twoje kryteria"),
            attrs={'class': 'message-title'})

    def find_ads(self):
        items = []
        divs = self.get_soup().find_all('div', attrs={'class': 'row row--property-list', 'data-id': True})
        for div in divs:
            item = div.find('a', attrs={'class': 'property_link'})
            items.append(item)

        ads = []
        for item in items:
            url = item.attrs['href']
            _id = url[url.rfind('-') + 1:]
            ad = self.create_ad(url, _id)
            ads.append(ad)

        return ads

    def get_house_url(self):
        return "https://www.morizon.pl/domy/najnowsze/wroclaw/{keyword}/?ps%5Bprice_from%5D={priceMin}&ps%5Bprice_to%5D={priceMax}"

    def get_apart_url(self):
        return "https://www.morizon.pl/mieszkania/najnowsze/wroclaw/{keyword}/?ps%5Bprice_from%5D={priceMin}&?ps%5Bprice_to%5D={priceMax}&ps%5Bliving_area_from%5D=80"
