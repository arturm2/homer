"""
Main Captors' orchestrator: runs a handful asynchronously
"""
import asyncio

import aiohttp

from homer.ad import Ad
from .gratka import GratkaCaptor
from .gumtree import GumtreeCaptor
from .morizon import MorizonCaptor
from .otodom import OtodomCaptor


class Ripper:
    """
    Main Captors' orchestrator: runs captors asynchronously
    """

    captors = [OtodomCaptor, MorizonCaptor, GumtreeCaptor, GratkaCaptor]

    def __init__(self, config):
        """
        :parameter config: configuration dictionary
        """
        self.config = config

    async def capture_ads(self):
        """
        Starts captors asynchronously and returns combined result
        """
        async with aiohttp.ClientSession() as http_session:
            # for each website we create a list of urls to capture and execute them consecutively
            # otherwise, we'll get host disconnected exceptions (too many requests)
            coros = []
            for captorClass in self.captors:
                tasks = []
                for kind in [Ad.KIND_APART, Ad.KIND_HOUSE]:
                    for district in self.config["districts"]:
                        district_map = self.config.get(captorClass.__name__ + "_map", None)
                        captor = captorClass(http_session, kind, district["name"], district["price_range"], district_map=district_map)
                        tasks.append(captor)

                coros.append(Ripper.execute(tasks))

            result = await asyncio.gather(*coros)
            ads = [ad for sublist in result for ad in sublist]
            return self._dedupe(ads)

    @staticmethod
    async def execute(tasks):
        """
        Executes a list of tasks one by one.
        Returns combined results (list of ads)
        """
        result = []
        for task in tasks:
            ads = await task.capture()
            result.extend(ads)
        return result

    @staticmethod
    def _dedupe(ads):
        """
        Deduplicates a list of ads by url
        """
        deduped = []
        for ad in ads:
            if ad.url not in [a.url for a in deduped]:
                deduped.append(ad)
        return deduped
