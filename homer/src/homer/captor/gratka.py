"""
Gratka implementation of Captor
"""
import re

from .captor import Captor


class GratkaCaptor(Captor):
    """
    Gratka implementation of Captor
    """

    site_name = 'gratka'

    def find_empty_marker(self):
        return self.get_soup().find(
            name='p',
            text=re.compile(u"Zwiększyliśmy zakres wyszukiwania, aby wyświetlić oferty zbliżone do Twoich wymagań."),
            attrs={'class': 'content__emptyListInfo'})

    def find_ads(self):
        items = self.get_soup().find_all('a', attrs={'class': 'teaserEstate__anchor'})
        ads = []
        for item in items:
            url = item.attrs['href']
            _id = url[url.rfind('/') + 1:]
            ad = self.create_ad(url, _id)
            ads.append(ad)
        return ads

    def get_house_url(self):
        return "https://gratka.pl/nieruchomosci/domy/wroclaw/{keyword}/sprzedaz?cena-calkowita:min={priceMin}&cena-calkowita:max={priceMax}&sort=newest"

    def get_apart_url(self):
        return "https://gratka.pl/nieruchomosci/mieszkania/wroclaw/{keyword}/sprzedaz?powierzchnia-w-m2:min=80&cena-calkowita:min={priceMin}&cena-calkowita:max={priceMax}&sort=newest"
