"""
Base class for all captors module
"""
import asyncio
from datetime import datetime
from abc import ABC, abstractmethod
import logging

from bs4 import BeautifulSoup

from homer.ad import Ad


class Captor(ABC):
    """
    Base class for all captors
    """

    site_name = None
    """
    ad website name
    """

    def __init__(self, http_session, kind, keyword, price_range, district_map=None):
        """
        :parameter http_session: aiohttp.ClientSession
        :parameter kind: Ad.kind (house or apartment)
        :parameter keyword: district to search for
        :parameter price_range: price range to search for
        :parameter name_map: optional district name map
        """
        self.http_session = http_session
        self.log = logging.getLogger(self.__class__.__name__)
        self.kind = kind
        self.keyword = keyword
        self.price_range = price_range
        self.content = None
        self.soup = None
        self.district_map = district_map

    @abstractmethod
    def find_empty_marker(self):
        """
        Finds an element indicating empty result set
        """

    @abstractmethod
    def find_ads(self):
        """
        Finds ad elements and returns list od Ads
        """

    @abstractmethod
    def get_apart_url(self):
        """
        Gets the url template for Ad.KIND_APART
        """

    @abstractmethod
    def get_house_url(self):
        """
        Gets the url template for Ad.KIND_HOUSE
        """

    def get_url(self, **kwargs):
        """
        Assembles url for kind, keyword and price range
        """
        url = self.get_apart_url() if self.kind == Ad.KIND_APART else self.get_house_url()
        return url.format(
            keyword=self.keyword,
            priceMin=self.price_range[0],
            priceMax=self.price_range[1],
            **kwargs
        )

    def get_soup(self):
        """
        Beautiful soup 4 getter
        """
        if not self.soup:
            self.soup = BeautifulSoup(self.content, 'html.parser')
        return self.soup

    async def download_content(self):
        """
        Downloads html for kind and keyword
        """
        url = self.get_url()
        response = await self.http_session.get(url, headers=self.headers)
        self.content = await response.text('utf-8')

        self.log.debug("Downloaded page for %s@%s: %s", self.kind, self.keyword, url)

    async def capture(self):
        """
        Main captor entry point: downloads and parses single result page (for kind and keyword)
        """
        result = []

        # noinspection PyBroadException
        try:
            await self.download_content()
        except Exception:
            self.log.exception("Unexpected error while downloading content for %s@%s", self.kind, self.keyword)
            return result

        try:
            result = self.parse_content()
        except Exception:
            self.log.exception("Unexpected error while parsing content of %s@%s", self.kind, self.keyword)

        return result

    def parse_content(self):
        """
        Parses content of downloaded page and finds ads.
        """
        if self.find_empty_marker():
            return []
        ads = self.find_ads()

        self.log.debug("Parsed %s@%s page, found %d ads", self.kind, self.keyword, len(ads))
        return ads

    def create_ad(self, url, _id):
        """
        Creates Ad with a few common values prepopulated
        """
        return Ad(self.kind, self.site_name, url, _id, self.keyword, datetime.now())

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'pl,en;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
        'Upgrade-Insecure-Requests': '1'
    }
