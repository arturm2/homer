"""
Gumtree implementation of Captor
"""
import re

from .captor import Captor


class GumtreeCaptor(Captor):
    """
    Gumtree implementation of Captor
    """

    site_name = 'gumtree'

    def find_empty_marker(self):
        return self.get_soup().find(
            name='div',
            text=re.compile(u"Niestety, nie znaleźliśmy żadnych wyników"),
            attrs={'class': 'message'})

    def find_ads(self):
        items = self.get_soup().select("a.href-link.tile-title-text")
        ads = []
        for item in items:
            url = self.home_url[:-1] + item.attrs['href']
            _id = url[url.rfind('/') + 1:]
            ad = self.create_ad(url, _id)
            ads.append(ad)
        return ads

    home_url = 'https://www.gumtree.pl/'

    def get_house_url(self):
        return "https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/dom/v1c9073l3200114a1dwp1?q={keyword}&pr={priceMin},{priceMax}"

    def get_apart_url(self):
        return "https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/mieszkanie/v1c9073l3200114a1dwp1?q={keyword}&pr={priceMin},{priceMax}"
