
import json
import os
import unittest
from asyncio import Future
from unittest.mock import Mock

from homer import Runner, Ad


def get_fake_ads(unique=True):
    """
    Loads a list of Ads from a file
    """
    filename = "fake_ads_unique.json" if unique else "fake_ads.json"
    filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", filename)
    with open(filepath) as f:
        content = f.read()
    return json.loads(content, object_hook=Ad.from_dict)


def get_ripper_mock():
    """
    Creates a Ripper mock with a handful of Ads loaded from a file
    """
    fut = Future()
    fut.set_result(get_fake_ads(unique=True))
    ripper = Mock()
    ripper.capture_ads.return_value = fut   # capture_ads is async, we need to use a Future
    return ripper


class RunnerTestGo(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(RunnerTestGo, self).__init__(*args, **kwargs)

        self.ripper = get_ripper_mock()
        self.storage = Mock()
        self.formatter = Mock()
        self.notifier = Mock()
        self.runner = Runner(self.ripper, self.storage, self.notifier, self.formatter)

    @staticmethod
    def _find_some_new_urls(urls):
        """
        Returns a list with every second element of the urls
        """
        return urls[::2]

    @staticmethod
    def _find_no_new_urls(urls):
        return []

    def test_go_some(self):
        self.storage.find_new_urls.side_effect = self._find_some_new_urls

        self.runner.go()

        self.storage.find_new_urls.assert_called_once()
        self.assertEqual(len(self.storage.find_new_urls.call_args[0][0]), 9)

        self.storage.store_ads.assert_called_once()
        self.assertEqual(len(self.storage.store_ads.call_args[0][0]), 5)

        self.formatter.get_text.assert_called_once()
        self.notifier.notify.assert_called_once()

    def test_go_empty(self):
        self.storage.find_new_urls.side_effect = self._find_no_new_urls

        self.runner.go()

        self.storage.find_new_urls.assert_called_once()
        self.assertEqual(len(self.storage.find_new_urls.call_args[0][0]), 9)

        self.storage.store_ads.assert_not_called()
        self.formatter.get_text.assert_not_called()
        self.notifier.notify.assert_not_called()
