import unittest

from homer.ad import Ad
from homer.captor import GumtreeCaptor
from homer.test.captor.captor import CaptorTest


class GumtreeCaptorTest(CaptorTest):

    def __init__(self, *args, **kwargs):
        super(GumtreeCaptorTest, self).__init__(*args, **kwargs)
        self.gumtree = GumtreeCaptor(None, Ad.KIND_HOUSE, 'any-keyword', (100000, 200000))

    def test_result_empty(self):
        self.gumtree.content = self.get_content('gumtree-empty.html')
        empty = self.gumtree.find_empty_marker()

        self.assertIsNotNone(empty)

    def test_result_ads(self):
        self.gumtree.content = self.get_content('gumtree-ads.html')
        empty = self.gumtree.find_empty_marker()

        self.assertIsNone(empty)

    def test_parse_ads(self):
        self.gumtree.content = self.get_content('gumtree-ads.html')
        result = self.gumtree.parse_content()

        self.assertEqual(8, len(result))
        self.assertEqual('1001951687910910468896009', result[0].id)
        self.assertEqual('https://www.gumtree.pl/a-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/ladny-dom-blisko-parku-na-biskupinie/1001951687910910468896009', result[0].url)
        self.assertEqual('1006424180720912511400109', result[-1].id)
        self.assertEqual('https://www.gumtree.pl/a-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/w-prestizowej-lokalizacji/1006424180720912511400109', result[-1].url)

    def test_parse_empty(self):
        self.gumtree.content = self.get_content('gumtree-empty.html')
        result = self.gumtree.parse_content()

        self.assertEqual(0, len(result))

    def test_get_url_house(self):
        url = self.gumtree.get_url()

        self.assertEqual(url, "https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/dom/v1c9073l3200114a1dwp1?q=any-keyword&pr=100000,200000")

    def test_get_url_apart(self):
        self.gumtree.kind = Ad.KIND_APART
        url = self.gumtree.get_url()

        self.assertEqual(url, "https://www.gumtree.pl/s-mieszkania-i-domy-sprzedam-i-kupie/wroclaw/mieszkanie/v1c9073l3200114a1dwp1?q=any-keyword&pr=100000,200000")

if __name__ == '__main__':
    unittest.main()
