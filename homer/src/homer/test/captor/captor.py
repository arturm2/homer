import os
import unittest

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class CaptorTest(unittest.TestCase):

    @staticmethod
    def get_content(filename):
        path = os.path.join(THIS_DIR, "data", filename)
        with open(path, 'r', encoding='utf-8') as f:
            content = f.read()
        return content
