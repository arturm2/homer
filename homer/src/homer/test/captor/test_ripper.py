import unittest

from homer.captor import Ripper
from homer.test.test_runner import get_fake_ads


class RunnerTestDedupe(unittest.TestCase):

    def test_dedupe(self):
        """
        Deduplication test
        """
        ads = get_fake_ads(unique=False)

        unique_ads = Ripper._dedupe(ads)

        self.assertEqual(len(unique_ads), 9)