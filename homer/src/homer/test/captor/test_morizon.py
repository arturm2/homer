import unittest

from homer.ad import Ad
from homer.captor import MorizonCaptor
from homer.test.captor.captor import CaptorTest


class MorizonCaptorTest(CaptorTest):

    def __init__(self, *args, **kwargs):
        super(MorizonCaptorTest, self).__init__(*args, **kwargs)
        self.morizon = MorizonCaptor(None, Ad.KIND_HOUSE, 'any-keyword', (100000, 200000))

    def test_result_empty(self):
        self.morizon.content = self.get_content('morizon-empty.html')
        empty = self.morizon.find_empty_marker()

        self.assertIsNotNone(empty)

    def test_result_ads(self):
        self.morizon.content = self.get_content('morizon-ads.html')
        empty = self.morizon.find_empty_marker()

        self.assertIsNone(empty)

    def test_parse_ads(self):
        self.morizon.content = self.get_content('morizon-ads.html')
        result = self.morizon.parse_content()

        self.assertEqual(11, len(result))
        self.assertEqual('mzn2033631044', result[0].id)
        self.assertEqual('https://www.morizon.pl/oferta/sprzedaz-dom-wroclaw-srodmiescie-235m2-mzn2033631044', result[0].url)
        self.assertEqual('mzn2030727320', result[-1].id)
        self.assertEqual('https://www.morizon.pl/oferta/sprzedaz-dom-wroclaw-srodmiescie-400m2-mzn2030727320', result[-1].url)

    def test_parse_empty(self):
        self.morizon.content = self.get_content('morizon-empty.html')
        result = self.morizon.parse_content()

        self.assertEqual(0, len(result))

    def test_get_url_house(self):
        url = self.morizon.get_url()

        self.assertEqual(url, "https://www.morizon.pl/domy/najnowsze/wroclaw/any-keyword/?ps%5Bprice_from%5D=100000&ps%5Bprice_to%5D=200000")

    def test_get_url_apart(self):
        self.morizon.kind = Ad.KIND_APART
        url = self.morizon.get_url()

        self.assertEqual(url, "https://www.morizon.pl/mieszkania/najnowsze/wroclaw/any-keyword/?ps%5Bprice_from%5D=100000&?ps%5Bprice_to%5D=200000&ps%5Bliving_area_from%5D=80")


if __name__ == '__main__':
    unittest.main()
