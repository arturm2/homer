import unittest

from homer.ad import Ad
from homer.captor import OtodomCaptor
from homer.test.captor.captor import CaptorTest


class OtodomCaptorTest(CaptorTest):

    def __init__(self, *args, **kwargs):
        super(OtodomCaptorTest, self).__init__(*args, **kwargs)
        self.otodom = OtodomCaptor(None, Ad.KIND_HOUSE, 'sepolno', (100000, 200000))

    def test_result_empty(self):
        self.otodom.content = self.get_content('otodom-empty.html')
        empty = self.otodom.find_empty_marker()

        self.assertIsNotNone(empty)

    def test_result_ads(self):
        self.otodom.content = self.get_content('otodom-ads.html')
        empty = self.otodom.find_empty_marker()

        self.assertIsNone(empty)

    def test_parse_ads(self):
        self.otodom.content = self.get_content('otodom-ads.html')
        result = self.otodom.parse_content()

        self.assertEqual(7, len(result))
        self.assertEqual('45MG9', result[0].id)
        self.assertEqual('https://www.otodom.pl/oferta/dom-wolnostojacy-na-sepolnie-ID45MG9.html#540514e669', result[0].url)
        self.assertEqual('44ax4', result[-1].id)
        self.assertEqual('https://www.otodom.pl/oferta/komfortowy-dom-z-pieknym-ogrodem-na-sepolnie-ID44ax4.html#540514e669', result[-1].url)

    def test_parse_empty(self):
        self.otodom.content = self.get_content('otodom-empty.html')
        result = self.otodom.parse_content()

        self.assertEqual(0, len(result))

    def test_get_url_house(self):
        url = self.otodom.get_url()

        self.assertEqual(url, "https://www.otodom.pl/sprzedaz/dom/wroclaw/sepolno/?search%5Bfilter_float_price%3Afrom%5D=100000&search%5Bfilter_float_price%3Ato%5D=200000&search%5Bdist%5D=0&search%5Bdistrict_id%5D=5874&search%5Bsubregion_id%5D=381&search%5Bcity_id%5D=39&search%5Border%5D=created_at_first%3Adesc")

    def test_get_url_apart(self):
        self.otodom.kind = Ad.KIND_APART
        url = self.otodom.get_url()

        self.assertEqual(url, "https://www.otodom.pl/sprzedaz/mieszkanie/wroclaw/sepolno/?search%5Bfilter_float_price%3Afrom%5D=100000&search%5Bfilter_float_price%3Ato%5D=200000&search%5Bfilter_float_m%3Afrom%5D=80&search%5Bdist%5D=0&search%5Bdistrict_id%5D=5874&search%5Bsubregion_id%5D=381&search%5Bcity_id%5D=39&search%5Border%5D=created_at_first%3Adesc")


if __name__ == '__main__':
    unittest.main()
