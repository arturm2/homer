import unittest

from homer.ad import Ad
from homer.captor import GratkaCaptor
from homer.test.captor.captor import CaptorTest


class GratkaCaptorTest(CaptorTest):

    def __init__(self, *args, **kwargs):
        super(GratkaCaptorTest, self).__init__(*args, **kwargs)
        self.gratka = GratkaCaptor(None, Ad.KIND_HOUSE, 'any-keyword', (100000, 200000))

    def test_result_empty(self):
        self.gratka.content = self.get_content('gratka-empty.html')
        empty = self.gratka.find_empty_marker()

        self.assertIsNotNone(empty)

    def test_result_ads(self):
        self.gratka.content = self.get_content('gratka-ads.html')
        empty = self.gratka.find_empty_marker()

        self.assertIsNone(empty)

    def test_parse_ads(self):
        self.gratka.content = self.get_content('gratka-ads.html')
        result = self.gratka.parse_content()

        self.assertEqual(9, len(result))
        self.assertEqual('11876641', result[0].id)
        self.assertEqual('https://gratka.pl/nieruchomosci/dom-wroclaw-srodmiescie-biskupin-ul-biskupin/ob/11876641', result[0].url)
        self.assertEqual('11556377', result[-1].id)
        self.assertEqual('https://gratka.pl/nieruchomosci/dom-wroclaw-biskupin-ul-okolice-elsnera/ob/11556377', result[-1].url)

    def test_parse_empty(self):
        self.gratka.content = self.get_content('gratka-empty.html')
        result = self.gratka.parse_content()

        self.assertEqual(0, len(result))

    def test_get_url_house(self):
        url = self.gratka.get_url()

        self.assertEqual(url, "https://gratka.pl/nieruchomosci/domy/wroclaw/any-keyword/sprzedaz?cena-calkowita:min=100000&cena-calkowita:max=200000&sort=newest")

    def test_get_url_apart(self):
        self.gratka.kind = Ad.KIND_APART
        url = self.gratka.get_url()

        self.assertEqual(url,
                         "https://gratka.pl/nieruchomosci/mieszkania/wroclaw/any-keyword/sprzedaz?powierzchnia-w-m2:min=80&cena-calkowita:min=100000&cena-calkowita:max=200000&sort=newest")


if __name__ == '__main__':
    unittest.main()
