import datetime
import json
import unittest

from homer.ad import Ad


class AdTest(unittest.TestCase):

    def test_to_json(self):
        ad = Ad(Ad.KIND_APART, 'gratka', 'http://gratka.pl/path', 'abc123', 'district', datetime.datetime(2020, 2, 2, 12, 0, 0, 0))

        ad_json = json.dumps(ad, default=Ad.to_json, sort_keys=True, indent=None)

        self.assertEqual(ad_json, '{"discovered_at": "2020-02-02T12:00:00", "id": "abc123", "keyword": "district", "kind": "APART", "site": "gratka", "url": "http://gratka.pl/path"}')

    def test_from_dict(self):
        ad_json = '{"discovered_at": "2020-02-02T12:00:00.000", "id": "abc123", "keyword": "district", "kind": "APART", "site": "gratka", "url": "http://gratka.pl/path"}'

        ad = json.loads(ad_json, object_hook=Ad.from_dict)

        self.assertEqual(ad.kind, Ad.KIND_APART)
        self.assertEqual(ad.site, 'gratka')
        self.assertEqual(ad.url, 'http://gratka.pl/path')
        self.assertEqual(ad.id, 'abc123')
        self.assertEqual(ad.keyword, 'district')
        self.assertEqual(ad.discovered_at, datetime.datetime(2020, 2, 2, 12, 0, 0))

