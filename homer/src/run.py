
from homer import Runner
from homer.captor import Ripper
from homer.notifier import EmailNotifier, Formatter
from homer.storage import SqliteStorage
from homer.util import read_json_config, setup_logging, get_filepath


def build(config):
    ripper = Ripper(config["ripper"])
    storage = SqliteStorage(config["sqlite_storage"])
    notifier = EmailNotifier(config["email_sender"])
    formatter = Formatter()
    return Runner(ripper, storage, notifier, formatter)


logging_config = read_json_config(get_filepath(__file__, "logging.json"))
setup_logging(logging_config)

config = read_json_config(get_filepath(__file__, "config.json"))
runner = build(config)
runner.go()
