"""
Template for integration testing
"""

from unittest.mock import Mock

from homer import Runner
from homer.notifier import Formatter, EmailNotifier
from homer.util import read_json_config, setup_logging, get_filepath
from homer.test.test_runner import get_ripper_mock


def dry_run(config):
    """
    Dry run with fake ripper, fake storage, notifier prints to stdout.
    Use to test various stuff.
    """
    ripper = get_ripper_mock()  # Ads loaded from a file
    storage = Mock()
    storage.find_new_urls.side_effect = lambda urls: urls   # returns the same list of urls
    notifier = EmailNotifier(config['email_sender'])
    # notifier.notify.side_effect = print # prints to stdout
    formatter = Formatter()
    runner = Runner(ripper, storage, notifier, formatter)
    runner.go()


logging_config = read_json_config(get_filepath(__file__, "logging.json"))
setup_logging(logging_config)

config = read_json_config(get_filepath(__file__, "config.json"))

dry_run(config)
